August �enoa
Prokleta kliet
Vienac zabavi i pouci
Zagreb, 1874.

Alberto Weber, Hermina Berki�
Matija Gubec ili Selja�ka buna: vesela igra u jednom �inu
Sisak: S. J�nker, 1907.

Milan Dobrovoljac (pseud. �migavec)
Sela�ka buna: spopeval �migavec, kipce nacajhnal Maurovi�
Zagreb: Tisak i naklada Jugoslovenske �tampe, 1927.

Petar Grgec
Matija Gubec: borac i mu�enik za prava hrvatskih seljaka
Zagreb: Moderna socijalna knji�nica, 1936.

Miroslav Krle�a
Balade Petrice Kerempuha
Ljubljana: Pri Akademski zalo�bi, 1936.

Antun Jirou�ek
Velika selja�ka buna godine 1573.
Zagreb: Selja�ka sloga, 1941.

Ante Martinovi� (urednik)
Matija Gubec: mali zbornik o Hrvatskoj selja�koj buni god. 1573.
Zagreb: Selja�ka sloga, 1947.

Mirko �e�elj (ilustracije Boris Dogan)
Slike iz selja�ke bune 1573.
Zagreb: �kolska knjiga, 1973.